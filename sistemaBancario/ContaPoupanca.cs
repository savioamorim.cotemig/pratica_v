using System;

namespace sistemaBancario
{
    public class ContaPoupanca
    {
        private string numeroConta;
        private string nome;
        private string documento;
        private double saldo;
        Random rnd = new Random();

        public ContaPoupanca(){
            this.numeroConta = Convert.ToString(rnd.Next(1000000, 9999999));
            this.saldo = 0;        
        }

        public void setNumeroConta(string pNumeroConta){
            this.numeroConta = pNumeroConta;                
        }

        public void setNome(string pNome){
            this.nome = pNome;
        }

        public void setDocumento(string pDocumento){
            this.documento = pDocumento;
        }

        public string getNumeroConta(){
            return this.numeroConta;
        }

        public string getNome(){
            return this.nome;
        }

        public string getDocumento(){
            return this.documento;
        }

        public double getSaldo(){
            return this.saldo;
        }
        public void depositar(double pDepositar){
            this.saldo += pDepositar;        
        }
        public bool sacar(double pSacar){
            if(this.saldo < pSacar){
                return false;
            }

            this.saldo -= pSacar;
            return true;
        }
        public string getStatusConta(){
            string status;

            return status = "\nNome: " + this.getNome() + "\n" +
            "Documento: " + this.getDocumento() + "\n" +
            "Número da conta: " + this.getNumeroConta() + "\n" +
            "Saldo: R$" + this.getSaldo();    
        }
    }
}