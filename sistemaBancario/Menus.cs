using System.Threading;

namespace sistemaBancario
{
    public class Menus
    {
        ContaPoupanca contaP1 = new ContaPoupanca();
        ContaCorrente contaC1 = new ContaCorrente();
         public void menu(){

            bool exit = false;
            
            do{
                System.Console.Clear();
                System.Console.WriteLine("===========================");
                System.Console.WriteLine("      CAIXA ELETRONICO");
                System.Console.WriteLine("---------------------------");
                System.Console.WriteLine(" 1 | Conta Corrente");
                System.Console.WriteLine(" 2 | Conta Poupança");
                System.Console.WriteLine(" 3 | Sair");
                System.Console.WriteLine("---------------------------");
                System.Console.WriteLine("Digite alguma das opções acima: ");
                
                switch(System.Console.ReadLine()){
                    case "1":
                        subMenu("CORRENTE");
                    break;
                    case "2":
                        subMenu("POUPANCA");
                    break;
                    case "3":
                        System.Console.WriteLine("FINALIZANDO SISTEMA...");
                        Thread.Sleep(2000);
                        exit = true;
                    break;
                    default:
                        System.Console.Clear();
                        System.Console.WriteLine("Selecione uma opção do menu.");
                        Thread.Sleep(2000);
                    break;
                };
            }while(!exit);
        }

        public void subMenu(string pTipoConta){

            bool exit = false, retorno;
            string input;
            double input_double;

            do{
                System.Console.Clear();
                System.Console.WriteLine("===========================");
                System.Console.WriteLine("      CONTA " + pTipoConta);
                System.Console.WriteLine("---------------------------");
                System.Console.WriteLine(" 1 | Cadastrar nova conta");
                System.Console.WriteLine(" 2 | Depositar");
                System.Console.WriteLine(" 3 | Sacar");
                System.Console.WriteLine(" 4 | Verificar Saldo");
                System.Console.WriteLine(" 5 | Status da Conta");
                System.Console.WriteLine(" 6 | Sair");
                System.Console.WriteLine("---------------------------");
                System.Console.WriteLine("Digite alguma das opções acima: ");

                switch(System.Console.ReadLine()){

                    // CADASTRAR NOVA CONTA
                    case "1":
                        if(pTipoConta == "POUPANCA"){
                            
                            System.Console.Clear();
                            System.Console.Write("Digite seu nome: ");
                            input = System.Console.ReadLine();
                            contaP1.setNome(input);

                            System.Console.Clear();
                            System.Console.Write("Digite seu documento: ");
                            input = System.Console.ReadLine();
                            contaP1.setDocumento(input);

                            System.Console.Clear();
                            System.Console.WriteLine("=======================");
                            System.Console.WriteLine(" CONTA POUPANCA CRIADA ");
                            System.Console.WriteLine("-----------------------");
                            System.Console.WriteLine(contaP1.getStatusConta());
                            System.Console.WriteLine("Pressione qualquer tecla para continuar...");
                            System.Console.ReadLine();                             
                        }
                        else{

                            System.Console.Clear();
                            System.Console.Write("Digite seu nome: ");
                            input = System.Console.ReadLine();
                            contaC1.setNome(input);

                            System.Console.Clear();
                            System.Console.Write("Digite seu documento: ");
                            input = System.Console.ReadLine();
                            contaC1.setDocumento(input);

                            System.Console.Clear();
                            System.Console.WriteLine("=======================");
                            System.Console.WriteLine(" CONTA CORRENTE CRIADA ");
                            System.Console.WriteLine("-----------------------");
                            System.Console.WriteLine(contaC1.getStatusConta());
                            System.Console.WriteLine("Pressione qualquer tecla para continuar...");
                            System.Console.ReadLine();

                        }
                    break;

                    // DEPOSITAR
                    case "2":

                        System.Console.Clear();
                        System.Console.WriteLine("============================");
                        System.Console.WriteLine(" DIGITE O VALOR DO DEPÓSITO ");
                        System.Console.WriteLine("----------------------------");

                        if(pTipoConta == "POUPANCA"){
                            retorno = validaConta(contaP1.getNome());
                            if(!retorno){
                                System.Console.WriteLine("CONTA POUPANÇA NAO ENCONTRADA!!!");
                                Thread.Sleep(3000);
                                break;
                            }             
                        }
                        else{
                            retorno = validaConta(contaC1.getNome());
                            if(!retorno){
                                System.Console.WriteLine("CONTA CORRENTE NAO ENCONTRADA!!!");
                                Thread.Sleep(3000);
                                break;
                            }
                        }

                        input_double = System.Convert.ToDouble(System.Console.ReadLine());
                        if(pTipoConta == "POUPANCA"){
                            contaP1.depositar(input_double);             
                        }
                        else{
                            contaC1.depositar(input_double);      
                        }

                        System.Console.WriteLine("DEPOSITO DE R$ " + input_double + " REALIZADO COM SUCESSO");
                        Thread.Sleep(5000);

                    break;

                    // SACAR
                    case "3":

                        bool result;

                        System.Console.Clear();
                        System.Console.WriteLine("============================");
                        System.Console.WriteLine(" DIGITE QUANTO DESEJA SACAR ");
                        System.Console.WriteLine("----------------------------");

                        if(pTipoConta == "POUPANCA"){
                            retorno = validaConta(contaP1.getNome());
                            if(!retorno){
                                System.Console.WriteLine("CONTA POUPANÇA NAO ENCONTRADA!!!");
                                Thread.Sleep(3000);
                                break;
                            }             
                        }
                        else{
                            retorno = validaConta(contaC1.getNome());
                            if(!retorno){
                                System.Console.WriteLine("CONTA CORRENTE NAO ENCONTRADA!!!");
                                Thread.Sleep(3000);
                                break;
                            }
                        }

                        System.Console.WriteLine("Quanto deseja sacar?");
                        input_double = System.Convert.ToDouble(System.Console.ReadLine());

                        if(pTipoConta == "POUPANCA"){
                            result = contaP1.sacar(input_double);             
                        }
                        else{
                            result = contaC1.sacar(input_double);        
                        }

                        if(result){
                            System.Console.WriteLine("SAQUE DE R$ " + input_double + " REALIZADO COM SUCESSO");
                            Thread.Sleep(4000);        
                        }
                        else{
                            System.Console.WriteLine("SALDO INSUFICIENTE");
                            Thread.Sleep(3000);
                        }

                    break;

                    // VERIFICAR SALDO
                    case "4":

                        System.Console.Clear();
                        System.Console.WriteLine("================");
                        System.Console.WriteLine(" SALDO EM CONTA ");
                        System.Console.WriteLine("----------------");

                        if(pTipoConta == "POUPANCA"){
                            retorno = validaConta(contaP1.getNome());
                            if(!retorno){
                                System.Console.WriteLine("CONTA POUPANÇA NAO ENCONTRADA!!!");
                                Thread.Sleep(3000);
                                break;
                            }
                            System.Console.WriteLine("R$ " + contaP1.getSaldo());             
                        }
                        else{
                            retorno = validaConta(contaC1.getNome());
                            if(!retorno){
                                System.Console.WriteLine("CONTA CORRENTE NAO ENCONTRADA!!!");
                                Thread.Sleep(3000);
                                break;
                            }
                            System.Console.WriteLine("R$ " + contaC1.getSaldo());        
                        }
                        System.Console.WriteLine("Pressione qualquer tecla para continuar...");
                        System.Console.ReadKey();

                    break;

                    // STATUS DA CONTA
                    case "5":

                        System.Console.Clear();
                        System.Console.WriteLine("=================");
                        System.Console.WriteLine(" STATUS DA CONTA ");
                        System.Console.WriteLine("-----------------");

                        if(pTipoConta == "POUPANCA"){
                            retorno = validaConta(contaP1.getNome());
                            if(!retorno){
                                System.Console.WriteLine("CONTA POUPANÇA NAO ENCONTRADA!!!");
                                Thread.Sleep(3000);
                                break;
                            }
                            System.Console.WriteLine(contaP1.getStatusConta());             
                        }
                        else{
                            retorno = validaConta(contaC1.getNome());
                            if(!retorno){
                                System.Console.WriteLine("CONTA CORRENTE NAO ENCONTRADA!!!");
                                Thread.Sleep(3000);
                                break;
                            }
                            System.Console.WriteLine(contaC1.getStatusConta());        
                        }

                        System.Console.WriteLine("Pressione qualquer tecla para continuar...");
                        System.Console.ReadKey();
                        
                    break;

                    // SAIR DO MENU
                    case "6":
                    exit = true;
                    break;
                    default:
                    System.Console.WriteLine("Selecione uma opção do menu.");
                    break;
                };
            }while(!exit);
        }

        public bool validaConta(object nomeBusca){
            if(nomeBusca == null){
                return false;    
            }
            return true;
        }

    }
}